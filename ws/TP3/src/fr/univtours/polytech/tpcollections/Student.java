package fr.univtours.polytech.tpcollections;

import java.util.HashMap;
import java.util.Map;

public class Student {

	private String name;

	private Map<Subject, Notes> notesList;

	public Student(String name) {
		this.name = name;
		this.notesList = new HashMap<Subject, Notes>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Map<Subject, Notes> getNotesList() {
		return notesList;
	}

	public Double computeMean() {

		if (0 == this.notesList.size()) {
			return 0D;
		}

		Double mean = 0D;
		Double coeffSum = 0D;
		for (Subject matiere : this.notesList.keySet()) {
			mean += this.notesList.get(matiere).computeMean() * matiere.getCoefficient();
			coeffSum += matiere.getCoefficient();
		}
		return mean / coeffSum;
	}
	
	@Override
	public String toString() {
		return this.name;
	}
}
