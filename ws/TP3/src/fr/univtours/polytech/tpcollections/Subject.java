package fr.univtours.polytech.tpcollections;

public class Subject {

	private String name;

	private Double coefficient;

	public Subject(String name, Double coefficient) {
		this.name = name;
		this.coefficient = coefficient;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getCoefficient() {
		return coefficient;
	}

	public void setCoefficient(Double coefficient) {
		this.coefficient = coefficient;
	}
}
